import {Component} from '@angular/core';
import {Location} from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public location: Location) {
  }

  removeHeader(): boolean {
    let titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if (titlee === 'dashboard' || titlee.search('detail') > -1) {
      return false;
    } else {
      return true;
    }
  }
}
