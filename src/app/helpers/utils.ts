import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class Utils {


  constructor() {
  }


  toHttpParams(params: any) {
    return Object.getOwnPropertyNames(params).reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }

  counter(i: number): any {
    return new Array(i);
  }


  formatDateStringToDate(dateString): any {
    try {
      const year = dateString.split('T')[0].split('-')[0];
      const month = dateString.split('T')[0].split('-')[1];
      const day = dateString.split('T')[0].split('-')[2];

      const hour = dateString.split('T')[1].split(':')[0];
      const minute = dateString.split('T')[1].split(':')[1];

      return day + '-' + month + '-' + year + ' ' + hour + ':' + minute;
    } catch (e) {

    }
  }


}
