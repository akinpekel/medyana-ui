import {Injectable} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from './auth.service';
import {HttpClient} from '@angular/common/http';
import {patientData} from './app.states';


declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  webSocket: WebSocket;
  private token: any;
  updateLatestOrders: any = Subscription;
  ws: any;

  constructor(public auth: AuthService, private http: HttpClient) {
    this.token = this.auth.getToken();
  }

  public openJqueryWebSocket(): void {
    const self = this;
    $(document).ready(() => {
      self.ws = $.websocket('ws://95.216.41.47:8005/ws/');

      self.ws.onopen = function () {
        console.log('open ws');
      };

      self.ws.onmessage = function (e) {
        self.handleData(e.data);
      };

      self.ws.onclose = function (e) {
        console.log('close', e);
        setTimeout(() => {
          if (self.token) {
            self.openJqueryWebSocket();
          }
        }, 3000);
      };
    });
  }

  public handleData(data): void {
    try {
      data = JSON.parse(data);
      if (data.length > 0) {
        console.log(data);
        patientData.next(data);
      }
    } catch (e) {
    }
  }

  public sendMessageWebSocket(message): void {
    const _message = {
      '$type': message,
    };
    if (this.ws) {
      this.ws.send(_message);
    }
  }

  public closeWebSocket(): void {
    if (this.ws) {
      this.ws.close();
    }
  }
}
