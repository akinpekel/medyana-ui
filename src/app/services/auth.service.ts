import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Utils} from '../helpers/utils';


@Injectable()
export class AuthService {
  user = null;
  public isAuthenticated = false;
  private apiHost = `${environment.host}`;

  constructor(private http: HttpClient, private utils: Utils) {
  }

  getToken(): any {
    return localStorage.getItem('token');
  }

  getUser(): any {
    return this.user;
  }

  login(data: any): any {
    return this.http.post(this.apiHost + '/api/login/', this.utils.toHttpParams(data), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    }).toPromise().then(response => {
      // @ts-ignore
      if (response.status) {
        // @ts-ignore
        localStorage.setItem('token', response.data.token);
      }
      return response;
    }, onerror => {
      return {status: false, data: {}, message: 'İşlem tamamlanamadı'};
    });
  }

  userAuthenticated(): boolean {
    return this.isAuthenticated || localStorage.getItem('token') != null;
  }

}
