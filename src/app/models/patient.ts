export class Patient {
  doctor_name?: string;
  doctor_note?: string;
  doctor_registration_code?: string;
  id: any;
  next_visit_date?: string;
  patient_firstname?: string;
  patient_gender?: string;
  patient_identification_number?: string;
  patient_lastname?: string;
  phone_number?: string;
  policlinic_code?: string;
  referance_code?: string;
  visit_date: any;
  patient_birth_date: any;
}
