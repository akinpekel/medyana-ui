import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {DetailComponent} from './pages/detail/detail.component';
import {AuthGuardService as AuthGuard} from './services/auth-guard.service';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent, data: {header: false}, canActivate: [AuthGuard]},
  {
    path: 'detail/:id',
    component: DetailComponent,
    data: {header: false},
    canActivate: [AuthGuard]
  },
  {path: 'login', component: LoginComponent, data: {header: true}},
  {
    path: '**',
    redirectTo: 'login'
    , data: {header: true}
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: false
    })
  ],
  exports: [],
})
export class AppRoutingModule {
}
