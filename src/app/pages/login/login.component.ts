import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('infoModal') infoModal: any;
  loginPageForm: any;
  public modal: NgbModalRef;
  modalText: string = '';

  constructor(private http: HttpClient, private modalService: NgbModal, private router: Router, private formBuilder: FormBuilder, public auth: AuthService) {
    if (this.auth.getToken() != null) {
      this.router.navigateByUrl('/dashboard');
    }

  }

  ngOnInit(): void {
    this.loginPageForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }


  onSubmit(): void {
    this.auth.login(this.loginPageForm.value).then(async response => {
      console.log(response.status);
      if (response.status) {
        this.router.navigateByUrl('/dashboard');
      } else {
        this.openInfoModal();
        this.modalText = response.message;
      }
    });
  }

  openInfoModal() {
    this.modal = this.modalService.open(this.infoModal, {size: 'sm'});
  }
}
