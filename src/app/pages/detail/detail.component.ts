import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../../services/auth.service';
import {Utils} from 'src/app/helpers/utils';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {Patient} from '../../models/patient';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @ViewChild('infoModal') infoModal: any;
  private id: any;
  public patient = new Patient();
  private apiHost = `${environment.host}`;
  public detailForm: FormGroup;
  public modal: NgbModalRef;
  modalTexts = {
    title: '',
    text: '',
    status: ''
  };
  public submitted: boolean = false;

  constructor(private http: HttpClient,
              private utils: Utils,
              private modalService: NgbModal,
              private auth: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getDetail();
    });
  }

  ngOnInit(): void {
    this.detailForm = this.formBuilder.group({
      patient_firstname: ['', Validators.required],
      patient_lastname: ['', Validators.required],
      patient_gender: ['', Validators.required],
      phone_number: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ])],
      referance_code: ['', Validators.required],
      patient_identification_number: ['', Validators.compose([
        Validators.required,
        Validators.minLength(11),
        Validators.maxLength(11)
      ])],
      patient_birth_date: ['', Validators.required],
      doctor_name: ['', Validators.required],
      doctor_registration_code: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(8)
      ])],
      policlinic_code: ['', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4)
      ])],
      visit_date: ['', Validators.required],
      next_visit_date: ['', Validators.required],
      doctor_note: ['', Validators.required],
    });
  }

  getDetail() {
    const data = {token: this.auth.getToken(), id: this.id};
    this.http.post(this.apiHost + '/api/detail-patients/', this.utils.toHttpParams(data), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    }).toPromise().then(res => {
      const response: any = res;
      if (response.status) {
        this.detailForm.patchValue({
          patient_firstname: response.data.patient_firstname,
          patient_lastname: response.data.patient_lastname,
          patient_gender: response.data.patient_gender,
          phone_number: response.data.phone_number,
          referance_code: response.data.referance_code,
          patient_identification_number: response.data.patient_identification_number,
          patient_birth_date: response.data.patient_birth_date,
          doctor_name: response.data.doctor_name,
          doctor_registration_code: response.data.doctor_registration_code,
          policlinic_code: response.data.policlinic_code,
          visit_date: response.data.visit_date,
          next_visit_date: response.data.next_visit_date,
          doctor_note: response.data.doctor_note
        });
      }
    }, onerror => {
      return {status: false, data: {}, message: 'İşlem tamamlanamadı'};
    });
  }

  get f(): any {
    return this.detailForm.controls;
  }


  cancel() {
    if (this.modal) {
      this.modal.dismiss();
    }
    this.router.navigateByUrl('/dashboard');
  }

  async savePatient() {
    if (this.detailForm.invalid) {
      this.modalTexts.status = 'error';
      this.modalTexts.title = 'Hata';
      this.modalTexts.text = 'Lütfen Tüm Alanları Doldurun.';
      this.submitted = true;
    } else {
      const data = this.detailForm.value;
      data['token'] = this.auth.getToken();
      data['id'] = this.id;
      await this.http.post(this.apiHost + '/api/update-patients/', this.utils.toHttpParams(data), {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
      }).toPromise().then(res => {
        const response: any = res;
        if (response.status) {
          this.modalTexts.status = 'succses';
          this.modalTexts.title = 'Kayıt Başarılı';
          this.modalTexts.text = 'Hasta Kaydı Başarılı Bir Şekilde Güncellenmiştir.';
        } else {
          this.modalTexts.status = 'error';
          this.modalTexts.title = 'Hata';
          this.modalTexts.text = response.message;
        }
      }, onerror => {
        this.modalTexts.status = 'error';
        this.modalTexts.title = 'Hata';
        this.modalTexts.text = 'İşlem Tamamlanamadı.';
      });
    }
    this.modal = this.modalService.open(this.infoModal, {size: 'md'});
  }
}
