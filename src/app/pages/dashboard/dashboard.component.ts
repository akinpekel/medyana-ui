import {Component, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Utils} from '../../helpers/utils';
import {AuthService} from '../../services/auth.service';
import {WebSocketService} from '../../services/web-socket.service';
import {patientData} from '../../services/app.states';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  private apiHost = `${environment.host}`;
  patients: any;
  metadata: any;
  filter = {
    referance_code: '',
    patient_name: '',
    policlinic_code: '',
    doctor_name: '',
    visit_date: '',
    doctor_registration_code: ''

  };
  public page: any = 1;
  private patientDataSubscription: Subscription;


  constructor(private http: HttpClient,
              public utils: Utils,
              private auth: AuthService,
              private router: Router,
              private webSocketService: WebSocketService) {
    this.webSocketService.openJqueryWebSocket();
  }

  ngOnInit(): void {
    this.getPatients();
  }

  getDetail(id: string) {
    this.router.navigateByUrl('/detail/' + id);
  }

  getPatients(filterData: boolean = false) {
    const data = this.filter;
    data['token'] = this.auth.getToken();
    this.page = filterData ? 1 : this.page;
    data['page'] = this.page;
    this.http.post(this.apiHost + '/api/list-patients/', this.utils.toHttpParams(data), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    }).toPromise().then(res => {
      const response: any = res;
      if (response.status) {
        this.patients = response.data.patients;
        this.metadata = response.data.page;
        this.getWebSocketData();
      } else {
        this.patients = [];
      }
    }, onerror => {
      return {status: false, data: {}, message: 'İşlem tamamlanamadı'};
    });
  }

  changePage(page: number): void {
    this.page = page;
    this.getPatients();
  }

  getWebSocketData(): void {
    this.patientDataSubscription = patientData.subscribe(value => {
      if (value) {
        console.log(value);
        let changePageCount = false;
        let deleteCount = 0;
        for (const item of value) {
          console.log(item);
          if (this.page === 1) {
            this.patients.unshift(item.fields);
            console.log('unshift');
            if (this.patients.length > 10) {
              this.patients.splice(-1, 1);
              changePageCount = true;
              deleteCount++;
            }
          }
        }
        if (changePageCount) {
          this.metadata.page_count++;
          this.metadata.total += deleteCount;
        }
      }
    });
  }

  ngOnDestroy() {
    this.patientDataSubscription.unsubscribe();
    this.webSocketService.closeWebSocket();
  }

}
